<?php

/**
 * Set up happybrowser variable
 */
function _happybrowser_setup() {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $default_loc = $GLOBALS['base_path'] . path_to_theme() .'/happybrowser.html';
  variable_set('happybrowser_redirect_loc',
    variable_get('happybrowser_redirect_loc', $default_loc));
}
