Introduction 
============

HappyBrowser ensures that visitors using version 6 of Internet Explorer
will be redirected to a special page on their first visit to the site.
The module uses a session cookie to allow IE6 users to view the site
after seeing the warning page.

Two modules: happybrowser and happybrowser_js
----------

The module works by server-side PHP and optionally by client-side JS.  The
former minimizes intrusion on non-IE browsers, but doesn't work through a
server-side cache like Varnish. If you are running a cache of this sort,
enable the happybrowser_js module and disable the happybrowser module.

Deploying with a server-side cache
----------

If you run a server-side cache such as Varnish, you need to disable the
happybrowser module and enable happybrowser_js. Otherwise, Varnish may
cache the redirect intended for old IE browsers and serve it to other
clients.

Both modules use a session cookie called "happybrowser_seen", but it is
only necessary to send this cookie to Drupal if you are NOT using
happybrowser_js. As long as you are using happybrowser_js, it is safe to
strip this cookie at the server level.

Installation
============

 1. Create an IE6 splash page that looks like your theme and suggests
    alternative browsers. Be sure to include a link back to your site's
    home page.
 2. Place this splash page in your theme's folder and give it the name
    "happybrowser.html". For example, if your theme is "bloopy", you 
    might put your splash page at:
    "sites/all/themes/bloopy/happybrowser.html"

Other Configuration
===================

To redirect IE6 users to some other location, set the system variable
"happybrowser_redirect_loc" to the desired destination. There is no UI
to do this, but you can use drush:

  drush vset 'happybrowser_redirect_loc' '/path/to/desired/file.html'

